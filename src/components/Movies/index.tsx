import React from 'react'
import useFetch from 'fetch-suspense'

import Movie, { MovieType } from './Movie'
import './Movies.scss'

type MoviesProps = {
  searchTerm: string,
}

const OMDB_API_KEY = '61df079d'

const Movies: React.FC<MoviesProps> = ({ searchTerm }) => {
  // @ts-ignore
  const { Search: movies } = useFetch(`https://www.omdbapi.com/?s=${searchTerm}&apikey=${OMDB_API_KEY}`)

  return (
    <div className="movies">
      {(movies && movies.length > 0)
        ? movies.map((m: MovieType) => <Movie key={m.imdbID} {...m} />)
        : <p>No movies found!.</p>
      }
    </div>
  )
}

export default Movies
