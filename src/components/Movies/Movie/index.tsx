import React from 'react'
import './Movie.scss'

export type MovieType = {
  imdbID: string,
  Title: string,
  Poster: string,
  Year: string,
}

const Movie: React.FC<MovieType> = ({ Title, Poster, Year }) => {
  const placeholder = `https://via.placeholder.com/200x300?text=${Title}`

  return (
    <div className="movie">
      <h2>{ Title }</h2>
      <img alt={`The movie titled: ${Title}`} src={Poster !== 'N/A' ? Poster : placeholder} />
      <p>{ Year }</p>
    </div>
  )
}

export default Movie