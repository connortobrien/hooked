import React, { MouseEvent, useState } from 'react'

import './Search.scss'

type SearchProps = {
  setSearchTerm: (value: string) => void,
}

const Search: React.FC<SearchProps> = ({ setSearchTerm }) => {
  const [searchValue, setSearchValue] = useState('')
  const submit = (event: MouseEvent) => {
    event.preventDefault()
    setSearchTerm(searchValue)
  }

  return (
    <form className="search">
      <input type="text" value={searchValue} onChange={e => setSearchValue(e.target.value)} />
      <button type="submit" onClick={submit}>SEARCH</button>
    </form>
  )
}

export default Search