import React from 'react'
import './Header.scss'

type HeaderProps = {
  text: string,
}

const Header: React.FC<HeaderProps> = ({ text }) => (
  <header className="App-header">
    <h2>{ text }</h2>
  </header>
)

export default Header
