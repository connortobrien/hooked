import React from 'react'

const Loading: React.FC = () => (
  <span>loading...</span>
)

export default Loading
