import React, { useState, useTransition, Suspense } from 'react'

import Loading from '../Loading'
import Header from '../Header'
import Search from '../Search'
import Movies from '../Movies'
import './App.scss'

const App: React.FC = () => {
  const [searchTerm, setSearchTerm] = useState('')
  const [startTransition, isPending] = useTransition()

  const changeSearchTerm = (value: string) => {
    startTransition(() => {
      setSearchTerm(value)
    })
  }

  return (
    <div className="app">
      <Header text="HOOKED" />
      <Search setSearchTerm={changeSearchTerm} />
      <p className="app-intro">Sharing a few of our favorite movies</p>
      {isPending
        ? <Loading />
        : (
          <Suspense fallback={<Loading />}>
            {searchTerm && <Movies searchTerm={searchTerm} />}
          </Suspense>
        )
      }
    </div>
  )
}

export default App
